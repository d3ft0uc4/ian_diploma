# coding=utf-8
import numpy as np
import random
from math import *
from collections import defaultdict
from random import shuffle, randint
import configparser
import os
import sys
from math import factorial as fac


def binomial(x, y):
    try:
        binom = fac(x) // fac(y) // fac(x - y)
    except ValueError:
        binom = 0
    return binom


def bitfield(n, n_bits):
    """Формирует битовый вектор длины n_bits из целого числа n"""
    result = [int(digit) for digit in bin(n)[2:]]
    # Заполняем нулями слева до необходимой длины
    while len(result) < n_bits:
        result.insert(0, 0)
    return result


threshold = .9


def generate_vector(n, monoms=None):
    """Случайная генерация вектора коэффициетов АНФ от n переменных.
    :param n: Количество переменных
    :param monoms: Количество мономов - опциональный параметр.
    Если указан, то генерирует полином с указанным количеством мономов
    """
    if monoms:
        vec = [1] * monoms + [0] * (int(pow(2, n)) - monoms)
        shuffle(vec)
        return vec

    return [int(random.SystemRandom().getrandbits(1)) for _ in xrange(int(pow(2, n)))]


def as_view(lst):
    """Наглядная интерпретация АНФ полинома на основе вектора коэффициентов.
    :param lst: Вектор вхождений мономов
    """
    n = int(log(len(lst), 2))
    variables = ['x{}'.format(i) for i in xrange(n)]
    polynomial = []
    for idx, bit in enumerate(lst):
        # Если бит == 1, то соответствующий по порядку моном встречается в булевой функции
        if bit:
            var_occurs = []
            fld = bitfield(idx, n)
            if not sum(fld):
                var_occurs.append('1')
            else:
                for i, v in enumerate(fld):
                    if v:
                        var_occurs.append(variables[i])
            polynomial.append('*'.join(var_occurs))
    if not polynomial:
        polynomial.append('0')
    return ' + '.join(polynomial)


def hamming_matrix(k):
    """Генерация матрицы Хэмминга - матрицы,
        столбцы которой - записи двоичного вида чисел от 1 до (2^k)-1
    :param k:
    """
    rows = []
    for i in xrange(1, long(pow(2, k))):
        rows.append(bitfield(i, k))
    return np.transpose(np.matrix(rows))


def get_monom_matrix(lst):
    n = int(log(len(lst), 2))
    matrix = []
    for idx, bit in enumerate(lst):
        # Если бит == 1, то соответствующий по порядку моном встречается в булевой функции
        if bit:
            matrix.append(bitfield(idx, n))
    return np.matrix(matrix)


def get_monom_matrix_from_input(n, deg, monoms):
    matrix = []
    head = [1] * deg + [0] * (n - deg)
    shuffle(head)
    matrix.append(head)
    while len(matrix) < monoms:
        monom_degree = randint(0, deg)
        elem = [1] * monom_degree + [0] * (n - monom_degree)
        shuffle(elem)
        if elem not in matrix:
            matrix.append(elem)
    return np.matrix(matrix)


def get_anf_from_monom_matrix(matr):
    rep = []
    for row in matr:
        if sum(row) == 0:
            rep.append('1')
        else:
            var_occurs = []
            for idx,el in enumerate(row):
                if el:
                    var_occurs.append('x{}'.format(idx))
            rep.append('*'.join(var_occurs))
    return ' + '.join(rep)


def append_ones_column(matrix):
    m, n = matrix.shape
    A1 = np.matrix(np.c_[np.ones(m, dtype=np.uint8), matrix])
    return A1


def build_C_from_N(matrix):
    result = []
    groups = defaultdict(list)
    n = 0
    for row in matrix:
        r = row.tolist()[0]
        n = len(r)
        groups[r[0]].append(len(filter(lambda x: x != 0, r[1:])))
    for k in sorted(groups.keys()):
        _r = []
        g = groups[k]
        for i in xrange(1, n):
            _r.append(g.count(i))
        result.append(_r)
    return np.matrix(result)


def count_zeros(matrix):
    k, m = matrix.shape
    acc = 0
    for s in xrange(1, k + 1):
        for r in xrange(1, m + 1):
            acc += matrix.item(s - 1, r - 1) * pow(2, s - r + m - 1) * pow(-1, s)

    acc += pow(2, m)
    return int(acc) % int(pow(2, m))


def assertions(n, deg, monoms):
    assert n >= 0
    assert n >= deg
    assert monoms > 0
    available_monoms = 0
    for i in xrange(deg + 1):
        available_monoms += binomial(n, i)
    assert available_monoms >= monoms


delimiter = "=" * 140

if __name__ == '__main__':
    # _file = os.path.abspath(sys.argv[0])
    # path = os.path.dirname(_file)
    # config = configparser.ConfigParser()
    # config.read(os.path.join(path, 'config.ini'))
    n = int(raw_input("Enter n:"))
    deg = int(raw_input("Enter deg:"))
    monoms = int(raw_input("Enter monom count: "))
    assertions(n, deg, monoms)
    # vec = generate_vector(n, monoms=monoms)
    # print 'Input vector: ', vec
    # print delimiter
    # print 'Algebraic normal form: ', as_view(vec)
    print delimiter
    A = get_monom_matrix_from_input(n, deg, monoms)
    print 'Monom matrix A: '
    print A
    print delimiter
    print 'Algebraic normal form: ', get_anf_from_monom_matrix(A.tolist())
    A1 = append_ones_column(A)
    print delimiter
    print '        1'
    print 'Matrix A : '
    print A1
    print delimiter
    H = np.transpose(hamming_matrix(monoms))
    print 'Hamming matrix: '
    print H
    print delimiter
    N = H * A1
    print 'N matrix: '
    print N
    print delimiter
    print 'C matrix: '
    C = build_C_from_N(N)
    print C
    print delimiter
    print 'Counted zeros from C matrix: '
    print count_zeros(C)
    raw_input()
